# Project tools

Tools for project development and tests.

Collect into one repository to avoid copies in every using repository.

## Components

The current goal is support for PHP Versions >= 7.1 .

### Code style

See [squizlabs](https://github.com/squizlabs/PHP_CodeSniffer).

```yaml
toCheck:
  file: "phpcs.phar"
  version: "3.5.2"
toFix:
  file: "phpcbf.phar"
  version: "3.5.2"
```

### Security

Symfony security checker.

```yaml
version: "6.0"
```

### Tests

PHPUnit Tests.

```yaml
version: "7.0.3"
```

## Some shell commands

.. TODO ..
```bash
# All checks
composer checks

# One at a time, see composer.json "scripts"
```
