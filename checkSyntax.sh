#!/usr/bin/env sh
# Shell: (sh checkSyntax.sh)

# First simple syntax checks

binPHP=$(which php)
errorCount=0
vPwd=$(pwd)

rm -f ${vPwd}/temp-syntax-test-files.txt
find ${vPwd} -iname "*.php" -type f -print | grep -Eiv "(var|vendor)/" | sort > ${vPwd}/temp-syntax-test-files.txt

for line in `cat ${vPwd}/temp-syntax-test-files.txt`
do
  # For debug
  #echo "- $line"
  count=$($binPHP -l $line | grep -iv "no syntax errors" | wc -l)
  errorCount=$(( $count + $errorCount ))
done

rm -f ${vPwd}/temp-syntax-test-files.txt
if [ $errorCount -gt 0 ]
then
  exit 1;
fi
exit 0;
